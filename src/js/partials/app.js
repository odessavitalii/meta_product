;(function ($) {

    const $accent = $('[data-accent-counter]');
    const $accentText = $('[data-accent-toggle-text]');
    const $accentBtn = $('[data-add-accent]');
    const $accordion = $('.accordion');
    const $body = $("body");
    const $btnOrder = $("[data-btn-order]");
    const $btnToggleMenu = $("#btn-toggle-menu");
    const $blockToggle = $('[data-block-toggle]');
    const $btnTooltip = $('.btn-tooltip');
    const $btnFilterNotActive = $('[data-btn-filter-not-active]');
    const $customSearch = $('.custom-search');
    const $chart = $('.chart');
    const $chat = $('.chat');
    const $chatBody = $chat.find('.chat__body');
    const $cardBodyWithScroll = $('.card-body.overflow-y-auto');
    const $counters = $('.counter');
    const $dataDateTimePickerInline = $("[data-toggle='datepicker']");
    const $dataDateTimePicker = $('[data-date-time-picker]');
    const $dataDateTimePickerList = $('[data-date-time-picker-list]');
    const $dataItemsOrder = $('[data-item-order]');
    const $dataModalLike = $('[data-modal-like]');
    const $dataModalActivation = $('[data-modal-activation]');
    const $dataBtnModalActivation = $('[data-btn-modal-activation]');
    const $dataModalConversion = $('[data-modal-conversion]');
    const $dataBtnModalConversion = $('[data-btn-modal-convertion]');
    const $fancybox = $('[data-fancybox="gallery"]');
    const $formControlFile = $('.form-control-file');
    const $inputNao = $('.input-nao__field');
    const $inputMaskTel = $('[data-inputmask-tel]');
    const $inputMaskBDay = $('[data-inputmask-bDay]');
    const $inputMaskEmail = $('[data-inputmask-email]');
    const $inputForBlockToggle = $('[data-input-for-block-toggle]');
    const $letters = $('.letter-icon');
    const $menu = $('#menu');
    const $onScrollInputText = $('[data-onScroll-input-text]');
    const $progress = $('.progress');
    const $radioHide = $('[data-radio-hide]');
    const $radioLike = $('[data-radio-like]');
    const $rangeSlider = $(".js-range-slider");
    const $rangeSliderSingle = $(".js-range-slider-single");
    const $rangeSliderSingleLink = $('.js-range-slider-single-link');
    const $rangeSliderSingleInput = $('.js-range-slider-single-input');
    const $rangeSliderInputStart = $('.js-range-slider-input-start');
    const $rangeSliderInputEnd = $('.js-range-slider-input-end');
    const $rangeCalendar = $("#range-calendar");
    const $rating = $("[data-rating]");
    const $ratingOutput = $("[data-rating-output]");
    const $secMain = $('.sec-main');
    const $select = $('.select');
    const $selectNao = $('.select-nao');
    const $selectForBlockToggle = $('[data-select-for-block-toggle]');
    const $slickFor = $('.slider-for');
    const $slickNav = $('.slider-nav');
    const $slickOrder = $('.slider-order');
    const $slickDots = $('.slider-dots');
    const $slickDotsBtnGoToEnd = $('[data-slick-go-to-end]');
    const $slickDotsBtnRedirect = $('[data-slick-redirect]');
    const $sticky = $('.sticky');
    const $stickyBreadcrumbs = $('[data-sticky-breadcrumbs]');
    const $stickyTabBar = $('[data-sticky-tab-bar]');
    const $switch = $('.switch.switch-sm');
    const $switchIn = $('.switch-in');
    const $switcherNavInput = $('[data-switcher-nav-input]');
    const $switcherInputToggle = $('[data-switcher-input-toggle]');
    const $timer = $('[data-timer-display]');
    const $tooltip = $('.tooltip');
    const $secSlide = $('[data-sec-slider]');
    const $secBtnSlideToSignIn = $('[data-sec-btn-slide-to-signin]');
    const $secBtnSlideToSignUp = $('[data-sec-btn-slide-to-signup]');
    const $dataCalendarToggle = $('[data-calendar-toggle]');
    const $dataCalendarBtnToggle = $('[data-calendar-btn-toggle]');
    const $dataSecBtnToggle = $('[data-sec-btn-toggle]');

    if ($dataCalendarBtnToggle.length) {
        let headerHeight = $('.header').height();
        let paddingInit = $dataSecBtnToggle.css("padding-top");
        let toggleWords = {
            down: 'Календарь',
            up: 'Свернуть'
        };

        $dataCalendarBtnToggle.on('click', function () {
            const self = $(this);

            self.toggleClass('active');
            $dataCalendarToggle.slideToggle(350).toggleClass('hidden');

            if ($dataCalendarToggle.hasClass('hidden')) {
                $dataSecBtnToggle.css("padding-top", headerHeight + 24);
                self.find('span').text(toggleWords.down)
            } else {
                $dataSecBtnToggle.css("padding-top", paddingInit);
                self.find('span').text(toggleWords.up)
            }
        })
    }
    
    if ($secSlide.length && $secBtnSlideToSignIn.length && $secBtnSlideToSignUp.length) {
        $secSlide.slick({
            autoplay: false,
            autoplaySpeed: 5000,
            adaptiveHeight: false ,
            arrows: false,
            centerMode: false,
            dots: false,
            focusOnSelect: false,
            swipe: false,
            swipeToSlide: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        });

        $secBtnSlideToSignIn.on('click', function (e) {
            e.preventDefault();

            $secSlide.slick('slickNext');
        });

        $secBtnSlideToSignUp.on('click', function (e) {
            e.preventDefault();

            $secSlide.slick('slickPrev');
        })

    }

    let templateStar = `<svg version="1.1" id="Слой_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 512 491.5" style="enable-background:new 0 0 512 491.5;" xml:space="preserve">
                <path class="st0" d="M268.1,9.4l65.1,154.1L500,177.9c11.6,1,16.3,15.4,7.5,23L381,310.5l37.9,163c2.6,11.3-9.6,20.2-19.6,14.2
                \t\t\tL256,401.3l-143.3,86.4c-10,6-22.2-2.9-19.6-14.2l37.9-163L4.5,200.9c-8.8-7.6-4.1-22,7.5-23l166.7-14.3L243.9,9.4
                \t\t\tC248.4-1.3,263.6-1.3,268.1,9.4L268.1,9.4z"/></svg>`;

    $(window).on('load', function () {
        const $preloader = $(".preloader");

        $preloader.removeClass('loading');

        $inputNao.each(function (index, inputNao) {
            if ($(inputNao).val() !== '') {
                $(inputNao).addClass('input-nao__filled');
            } else {
                $(inputNao).removeClass('input-nao__filled');
            }
        });

        $('input').attr('autocomplete', 'false');
        $('form').attr('autocomplete', 'off');
    });

    $(window).on('scroll', function (event) {
        let scroll = $(window).scrollTop();

        if ($sticky.length) {
            if (scroll > 0) {
                $sticky.addClass('fixed');
            } else {
                $sticky.removeClass('fixed');
            }
        }

    });

    if ($switchIn.length) {
        const $input = $switchIn.find('input[type=checkbox]');
        const $inputChecked = $switchIn.find('input[type=checkbox]:checked');

        if ($inputChecked.length) {
            $inputChecked.closest('.switch-in').addClass('checked')
        }

        $input.on('change', function () {
            const self = $(this);
            if (self.prop('checked')) {
                self.closest('.switch-in').addClass('checked')
            } else {
                self.closest('.switch-in').removeClass('checked')
            }
        })
    }

    if ($btnOrder.length && $dataItemsOrder.length) {
        $dataItemsOrder.on('change', function () {
            const els = $dataItemsOrder.filter(function (index, item) {
                return $(this).val() !== null;
            });

            if (els.length > 0) {
                $blockToggle.find('button').removeAttr('disabled');
            }

        })
    }

    if ($dataDateTimePicker.length) {

        $dataDateTimePicker.datetimepicker({
            uiLibrary: 'bootstrap4',
            modal: true,
            footer: true,

        });

        $('.gj-picker .modal-footer .btn:nth-child(2)').on('click', function () {

            console.log($('.input-group-append .btn').val());

            if ($('.input-group-append .btn').prev('.form-control') !== 0 ) {
                $('.input-group-append .btn').html('');
            }
        });
    }

    if ($dataDateTimePickerList.length) {

        $dataDateTimePickerList.each((index, datepicker) => {

            $(datepicker).datetimepicker({
                uiLibrary: 'bootstrap4',
                modal: true,
                footer: true,
                change: (e) => {
                    console.log(e)
                },
            });

            $('.gj-picker .modal-footer .btn:nth-child(2)').text('Откликнуться')

        })
    }

    if ($dataDateTimePickerInline.length) {

        $dataDateTimePickerInline.datepicker({
            inline: true
        });
    }

    if ($select.length) {
        $select.on('click', '.select__placeholder', function () {
            const parent = $(this).closest('.select');

            if (!parent.hasClass('is-open')) {
                parent.addClass('is-open');
                $('.select.is-open').not(parent).removeClass('is-open');
            } else {
                parent.removeClass('is-open');
            }
        }).on('click', '.select__list > li', function () {
            const parent = $(this).closest('.select');
            const self = $(this);

            parent.removeClass('is-open').find('.select__placeholder').text($(this).text());
            parent.addClass('is-dirty');

            $('.select__list > li').removeClass('selected');
            self.addClass('selected');
        });
    }

    if ($selectNao.length) {

        $selectNao.on('click', '.select-nao__placeholder', function () {
            const parent = $(this).closest('.select-nao');

            if (!parent.hasClass('is-open')) {
                parent.addClass('is-open');
                $('.select.is-open').not(parent).removeClass('is-open');
            } else {
                parent.removeClass('is-open');
            }
        }).on('click', '.select-nao__list > li', function () {
            const parent = $(this).closest('.select-nao');
            const self = $(this);

            parent.removeClass('is-open').find('.select-nao__placeholder').text($(this).text());
            parent.addClass('is-dirty');

            $('.select-nao__list > li').removeClass('selected');
            self.addClass('selected');
        });
    }

    if ($menu.length && $btnToggleMenu.length) {

        $menu.mmenu({
            language: 'ru',
            extensions: {
                all: ["theme-white", "pagedim-black"],
                "(max-width: 549px)": ["fx-menu-slide"]
            },
            counters: true,
            dividers: {
                fixed: true
            },
            iconPanels: {
                add: true,
                hideDivider: true,
                hideNavbar: true,
                visible: "first"
            },
            navbar: {
                title: "Меню"
            },
            navbars: [{
                position: "top"
            }],
            searchfield: {
                "panel": true,
            },
            setSelected: {
                hover: true,
                parent: true
            },
            sidebar: {
                collapsed: {
                    use: 550,
                    hideNavbar: true,
                    hideDivider: true
                },
                expanded: 1430
            }
        });

        var $icon = $("#btn-toggle-menu");
        var API = $menu.data( "mmenu" );

        $icon.on( "click", function() {
            API.open();
        });

        API.bind( "open:finish", function() {
            setTimeout(function() {
                $icon.addClass( "is-active" );
            }, 100);
        });
        API.bind( "close:finish", function() {
            setTimeout(function() {
                $icon.removeClass( "is-active" );
            }, 100);
        });

        // setTimeout(function () {
        //     if ($('.mm-wrapper_opening').length && $('#menu')) {
        //         $body.removeClass('mm-wrapper_opening');
        //         $('#menu').removeClass('mm-menu_opened');
        //     }
        // }, 2000);

        // var api = $menu.data('mmenu');
        //
        // console.log("api", api);

        // api.bind('opened', function () {
        //     console.log("menu open");
        //     $btnToggleMenu.addClass('is-active');
        // }).bind('closed', function () {
        //     console.log("menu closed");
        //     $btnToggleMenu.removeClass('is-active');
        // });

        // var api = $menu.data( "mmenu" );
        //
        // api.bind( "openPanel:start", function( $panel ) {
        //     console.log( "This panel is now opening: #" + $panel.attr( "id" ) );
        // });
        // api.bind( "openPanel:finish", function( $panel ) {
        //     console.log( "This panel is now opened: #" + $panel.attr( "id" ) );
        // });

    }

    if ($customSearch.length) {
        const $input = $customSearch.find('.custom-search__input');
        const $dash = $customSearch.find('.custom-search__dash');

        $input.focusin(function() {
            $(this).addClass('active');
            $dash.addClass('move');
        });

        $input.focusout(function() {
            $(this).removeClass('active').val("");
            $dash.removeClass('move');
        });
    }

    if ($cardBodyWithScroll.length) {
        const win = $cardBodyWithScroll.find('.container');
        const doc = $cardBodyWithScroll;
        const progressBar = $accent.find('.counter__circle-value');
        const progressLabel = $accent.find('.counter__output');
        let current = 0;
        // const setValue = () => win.scrollTop();
        // const setMax = () => doc.height() - win.height();
        const setPercent = () => Math.round($cardBodyWithScroll.scrollTop() / ($cardBodyWithScroll.find('.container').height() - $cardBodyWithScroll.height()) * 100);

        progressLabel.text(setPercent() + '%');
        progressBar.attr('stroke-dasharray', (`${ setPercent() }, 100`));

        doc.on('scroll', () => {

            if (setPercent() === current || setPercent() > current) {
                current = setPercent();

                progressLabel.text(setPercent());
                progressBar.attr('stroke-dasharray', (`${ setPercent() }, 100`));
            }

            if (current >= 100) {
                $blockToggle.find('button').removeAttr('disabled');
            }
        });

        win.on('resize', () => {
            progressLabel.text(setPercent());
            progressBar.attr('stroke-dasharray', (`${ setPercent() }, 100`));
        });

        $accentBtn.on('click', function () {
            $accentText.text('акцепт получен');
            $('.tick').removeClass('tick-hidden');
        })
    }

    if ($secMain.length) {
        $.scrollAnimate = function (options, scrollJqueryElem) {
            const settings = $.extend({
                scrollDown: () => {
                },
                scrollUp: () => {
                }
            }, options);

            let scrollPosition = 0;
            let isScrollHeight = scrollJqueryElem.prop('scrollHeight') - scrollJqueryElem.prop('scrollTop') - scrollJqueryElem.prop('clientHeight') - 8;

            scrollJqueryElem.on('scroll', function () {
                let cursorPosition = $(this).scrollTop();

                if (cursorPosition === 0) {
                    settings.scrollUp();
                } else if (isScrollHeight <= cursorPosition && !!isScrollHeight) {
                    settings.scrollDown();
                } else if (cursorPosition > scrollPosition && (cursorPosition - scrollPosition) < 5) {
                    settings.scrollDown();
                } else if (scrollPosition > cursorPosition && (cursorPosition - scrollPosition) < -5) {
                    settings.scrollUp();
                }
                scrollPosition = cursorPosition;
            });
        };

        let handleScrollUp = debounce(function () {
            addClassToBody($stickyTabBar, 'sticky-tab-bar');
            addClassToBody($stickyBreadcrumbs, 'sticky-breadcrumbs');
        }, 2000);

        let handleScrollDown = debounce(function () {
            removeClassFromBody($stickyTabBar, 'sticky-tab-bar');
            removeClassFromBody($stickyBreadcrumbs, 'sticky-breadcrumbs');
        }, 2000);

        $.scrollAnimate({
            scrollDown: handleScrollUp,
            scrollUp: handleScrollDown
        }, $secMain);
    }

    if ($chat.length) {
        let scrollHeight = $chatBody.find('.container').prop('clientHeight');
        $chatBody.scrollTop(scrollHeight);
    }

    if ($btnFilterNotActive.length) {
        const elementsForToggle = $('.list-item:not(.list-item-not-active)');
        const contextBtn = {
            new: "Посмотреть новые",
            all: "Посмотреть все"
        };

        $btnFilterNotActive.on('click', function () {
            const self = $(this);

            if (self.hasClass('toggle')) {
                elementsForToggle.fadeIn();
                self.removeClass('toggle').find('[data-text-toggle]').text(contextBtn.new);
            } else {
                elementsForToggle.fadeOut();
                self.addClass('toggle').find('[data-text-toggle]').text(contextBtn.all);
            }
        })

    }

    if ($inputMaskTel.length) {
        $inputMaskTel.inputmask({
            "mask": "+7 (999) 999-99-99"
        })
    }

    if ($inputMaskEmail.length) {
        $inputMaskEmail.inputmask({
            mask: "*{1,20}@*{1,20}[.*{2,6}][.*{1,2}]",
            greedy: false,
            onBeforePaste: function (pastedValue, opts) {
                pastedValue = pastedValue.toLowerCase();
                return pastedValue.replace("mailto:", "");
            },
            definitions: {
                '*': {
                    validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                    casing: "lower"
                }
            }
        });
    }

    if ($inputMaskBDay.length) {
        $inputMaskBDay.inputmask({
            alias: "datetime",
            inputFormat: "dd/mm/yyyy"
        })
    }

    if ($switcherNavInput.length && $switcherInputToggle.length) {
        $switcherNavInput.on('change', function () {
            const self = $(this);
            let val = self.data('switcher-nav-input');

            $switcherInputToggle.each((index, input) => {
                let valInput = $(input).data('switcher-input-toggle');

                if (self.is(':checked')) {
                    if (val === valInput) {
                        $(input).removeAttr('disabled')
                    }
                } else {
                    if (val === valInput) {
                        $(input).attr('disabled', 'disabled');
                    }
                }
            })
        })
    }

    if ($tooltip.length && $btnTooltip.length) {
        $btnTooltip.on('click', function (e) {
            const self = $(this);
            let target = $(e.target);

            if (this.tagName.toLowerCase() === "a") {
                e.preventDefault();
            }

            if (!target.hasClass('btn-tooltip')) {

                while (target !== self) {

                    if (target.hasClass('btn-tooltip')) {

                        toggleTooltip(self);

                        return false;
                    }

                    target = target.parent();
                }

            } else {
                toggleTooltip(self);
            }
        });

        function toggleTooltip(current) {
            if (current.next('.tooltip').hasClass('open')) {
                $tooltip.removeClass('open');
            } else {
                $tooltip.removeClass('open');
                current.next('.tooltip').addClass('open');

                if (current.next('.tooltip-fade')) {
                    setTimeout(() => {
                        current.next('.tooltip-fade').removeClass('open');
                    }, 4000)
                }
            }
        }

        $(document).on('click', function (e) {

            if ($tooltip.hasClass('open')) {

                $tooltip.each((index, tooltip) => {

                    if (!$(tooltip).is(e.target) && $tooltip.has(e.target).length === 0 && $btnTooltip.has(e.target).length === 0 && !$btnTooltip.is(e.target)) {
                        $(tooltip).removeClass('open');
                    }

                });
            }
        })
    }

    if ($fancybox.length) {
        $fancybox.fancybox({
            opacity: true,
            loop: false
        });
    }

    if ($radioHide.length) {
        $radioHide.on('click', function () {
            const $closest = $(this).closest('.list-item');

            $closest.fadeOut(function () {
                $(this).remove();
            })
        });
    }

    if ($radioLike.length) {
        $radioLike.on('click', function (e) {
            const $closest = $(this).closest('.list-item');

            $dataModalLike.addClass('is-open');

            $(this).toggleClass('active');

            if ($rating.length) {

                $rating.rateYo({
                    rating: 0,
                    starWidth: "32px",
                    normalFill: "#bab2bf",
                    ratedFill: "#5e3080",
                    spacing   : "10px",
                    numStars: 5,
                    precision: 0,
                    starSvg: templateStar,
                    onInit: function (rating, rateYoInstance) {
                        // console.log('event onInit: rating', rating);
                    },
                    onSet: function (rating, rateYoInstance) {

                        console.log('event onSet: rating', rating);

                        $dataModalLike.removeClass('is-open');

                        $closest.fadeOut(function () {
                            $(this).remove();
                        });
                    },
                    onChange: function (rating, rateYoInstance) {
                        // console.log('event onChange: rating', rating);
                    }
                });
            }
        });
    }

    if ($dataModalActivation.length && $dataBtnModalActivation.length) {
        $dataBtnModalActivation.on('click', function (e) {
            toggleModal($dataModalActivation);
        });

        $dataModalActivation.find('.modal__close').on('click', function () {
            toggleModal($dataModalActivation);
        });
    }

    if ($dataModalConversion.length && $dataBtnModalConversion.length) {
        $dataBtnModalConversion.on('click', function (e) {
            toggleModal($dataModalConversion);
        });

        $dataModalConversion.find('.modal__close').on('click', function () {
            toggleModal($dataModalConversion);
        });
    }

    function toggleModal(current) {
        if (current.hasClass('is-open')) {

            current.removeClass('is-open');

        } else if (typeof(current.attr('data-fade')) !== 'undefined') {

            current.addClass('is-open');

            setTimeout(() => {
                current.removeClass('is-open');
            }, 4000)

        } else {

            $('.modal').removeClass('is-open');
            current.addClass('is-open');

        }
    }

    if ($ratingOutput.length) {

        $ratingOutput.rateYo({
            rating: 3.3,
            starWidth: "24px",
            normalFill: "#bab2bf",
            ratedFill: "#5e3080",
            spacing   : "3px",
            readOnly: true,
            numStars: 5,
            precision: 1,
            starSvg: templateStar,
            onInit: function (rating, rateYoInstance) {
                // console.log('event onInit: rating', rating);
            },
            onSet: function (rating, rateYoInstance) {

                console.log('event onSet: rating', rating);
            },
            onChange: function (rating, rateYoInstance) {
                // console.log('event onChange: rating', rating);
            }
        });
    }

    if ($counters.length) {
        counter();
    }

    if (accent.length) {
        accent();
    }

    if ($rangeCalendar.length) {
        let ru = 'ru';
        let currentDate = moment();
        let endDate = moment().add('months', 12);

        $rangeCalendar.rangeCalendar({
            lang: ru,
            theme: "default-theme",
            themeContext: this,
            startDate: currentDate,
            endDate: endDate,
            start: "+7",
            startRangeWidth: 1,
            minRangeWidth: 1,
            maxRangeWidth: 1,
            weekends: true,
            autoHideMonths: false,
            visible: true,
            trigger: 'click',

            changeRangeCallback: rangeChanged
        });

        function rangeChanged(target, range) {
            console.log("target: ", target, "range: ", range);
            return false;
        }
    }

    if ($rangeSlider.length) {
        $rangeSlider.ionRangeSlider({
            type: "double",
            min: 0,
            max: 24,
            from: 13,
            to: 18,
            step: 1,
            postfix: ".00",
            skin: 'round',
            hide_min_max: true,
            hide_min_to: true,
            onFinish: function (data) {
                // console.log(data);
                const $inputStart = $rangeSliderInputStart;
                const $inputEnd = $rangeSliderInputEnd;
                let from = data.from;
                let to = data.to;

                if (from < 10) {
                    $inputStart.text("0" + from + ".00");
                } else {
                    $inputStart.text(from + ".00")
                }

                if (to < 10) {
                    $inputEnd.text("0" + to + ".00")
                } else {
                    $inputEnd.text(to + ".00")
                }
            }
        });
    }

    if ($rangeSliderSingle.length) {
        $rangeSliderSingle.ionRangeSlider({
            min: 0,
            max: 24,
            from: 13,
            step: 1,
            postfix: ".00",
            skin: 'round',
            hide_min_max: true,
            hide_min_to: true,
            onChange: function (data) {
                // console.log(data);
                const $input = $('.js-range-slider-single-input.active');
                let from = data.from;

                if (from < 10) {
                    $input.text("0" + from + ".00")
                } else {
                    $input.text(from + ".00")
                }
            }
        });

        if (!$rangeSliderSingleInput.hasClass('active')) {
            $rangeSliderSingleInput.first().addClass('active')
        }

        $rangeSliderSingleLink.on('click', function (e) {
            e.preventDefault();

            $rangeSliderSingleInput.removeClass('active');
            $(this).find('.js-range-slider-single-input').addClass('active')
        })
    }

    if ($inputNao.length) {
        $inputNao.on('focus blur input', function () {

            if ($(this).val() !== '') {
                $(this).addClass('input-nao__filled');
            } else {
                $(this).removeClass('input-nao__filled');
            }
        });
    }

    if ($blockToggle.length && $inputForBlockToggle.length && $selectForBlockToggle.length) {

        $selectForBlockToggle.on('click', '.select__list > li', function () {

            const select = $('[data-select-for-block-toggle].is-dirty');

            const els = $('.input-nao__field').filter(function() {
                return $(this).val() !== "" && $(this).val() !== "0";
            });


            if (els.length > 0 && select.length === $selectForBlockToggle.length) {
                setTimeout(() => {
                    $blockToggle.find('button[type=submit]').removeAttr('disabled');
                })
            } else {
                $blockToggle.find('button[type=submit]').attr('disabled', 'disabled');
            }

        });


        $inputForBlockToggle.on('input', function () {

            const select = $selectForBlockToggle.filter((index, item) => {
                return $(item).find('.select__placeholder').text() !== "" && $(item).find('.select__placeholder').text() !== "Выбирите..."
            });

            const els = $('.input-nao__field').filter(function() {
                return $(this).val() !== "" && $(this).val() !== "0";
            });


            if (els.length > 0 && select.length === $selectForBlockToggle.length) {
                $blockToggle.find('button[type=submit]').removeAttr('disabled');
            } else {
                $blockToggle.find('button[type=submit]').attr('disabled', 'disabled');
            }
        })

    }

    if ($blockToggle.length && $inputForBlockToggle.length && !$formControlFile.length && !$selectForBlockToggle.length) {

        $inputForBlockToggle.on('input', function () {

            const els = $('.input-nao__field').filter(function() {
                return $(this).val() !== "" && $(this).val() !== "0";
            });

            if (els.length > 0) {
                $blockToggle.find('button[type=submit]').removeAttr('disabled');
            } else {
                $blockToggle.find('button[type=submit]').attr('disabled', 'disabled');
            }
        })

    }

    if ($blockToggle.length && $inputForBlockToggle.length && $formControlFile.length && !$selectForBlockToggle.length) {

        $inputForBlockToggle.on('input', function () {

            const els = $('.input-nao__field').filter(function() {
                return $(this).val() !== "" && $(this).val() !== "0";
            });

            const elsFile = $formControlFile.find('.form-control-file__item');

            if (els.length > 0 && elsFile.length) {
                $blockToggle.find('button[type=submit]').removeAttr('disabled');
            } else {
                $blockToggle.find('button[type=submit]').attr('disabled', 'disabled');
            }
        })
    }

    if ($chart.length) {
        Highcharts.chart('chart', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                margin: [0, 0, 0, 0],
                spacing: [0, 0, 0, 0],
            },
            title: {
                text: ''
            },
            tooltip: {
                padding: 4,
                distance: 24,
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: false,
                    animation: {
                        duration: 500
                    },
                    slicedOffset: 4,
                },
                series: {
                    states: {
                        inactive: {
                            opacity: 1
                        },
                        hover: {
                            enabled: true,
                            halo: {
                                opacity: 0.5,
                                size: 4
                            }
                        },
                        select: {
                            enabled: true,
                            halo: {
                                opacity: 0.5,
                                size: 4
                            }
                        }
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                name: 'Brands',
                colorByPoint: true,
                data: [{
                    name: 'Chrome',
                    y: 61.41,
                    sliced: false,
                    selected: false
                }, {
                    name: 'Internet Explorer',
                    y: 11.84
                }, {
                    name: 'Firefox',
                    y: 10.85
                }, {
                    name: 'Edge',
                    y: 4.67
                }, {
                    name: 'Safari',
                    y: 4.18
                }, {
                    name: 'Other',
                    y: 7.05
                }]
            }],
        });
    }

    if ($switch.length) {
        const switchInput = $switch.find('input[type=checkbox]');
        let isChecked = switchInput.prop('checked');
        const $counters = $("#counters");

        if (isChecked) {
            toggleAnimate($counters, "show");
        }

        switchInput.on('change', function () {
            isChecked = switchInput.prop('checked');

            if (isChecked) {
                toggleAnimate($counters, "show");
            } else {
                toggleAnimate($counters, "hide");
            }
        });
    }

    if ($slickDots.length) {
        $slickDots.on('init', handleSlickForInit);

        $slickDots.slick({
            autoplay: false,
            autoplaySpeed: 5000,
            adaptiveHeight: true ,
            arrows: false,
            centerMode: false,
            dots: true,
            focusOnSelect: true,
            swipe: true,
            swipeToSlide: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
        })
            .on('swipe', handleSlickForSwipe)
            .on('beforeChange', handleSlickForBeforeChange)
            .on('afterChange', handleSlickForAfterChange);

        $slickDotsBtnGoToEnd.on('click', function (e) {
            e.preventDefault();

            $slickDots.slick('slickGoTo', 3);
            $slickDotsBtnGoToEnd.hide();
            // $slickDotsBtnRedirect.removeAttr('hidden')
        })
    }

    if ($slickOrder.length) {

        $slickOrder.on('init', handleSlickForInit);

        $slickOrder.slick({
            adaptiveHeight: true ,
            arrows: false,
            centerMode: false,
            dots: false,
            focusOnSelect: true,
            swipe: true,
            swipeToSlide: true,
            infinite: true,
            centerPadding: '40px',
            slidesToShow: 1,
            slidesToScroll: 1,
        })
            .on('swipe', handleSlickForSwipe)
            .on('beforeChange', handleSlickForBeforeChange)
            .on('afterChange', handleSlickForAfterChange);
    }

    if ($slickFor.length && $slickNav.length) {

        $slickFor.on('init', handleSlickForInit);

        $slickFor.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            asNavFor: '.slider-nav',
            adaptiveHeight: false,
            infinite: true
        })
            .on('swipe', handleSlickForSwipe)
            .on('beforeChange', handleSlickForBeforeChange)
            .on('afterChange', handleSlickForAfterChange);

        $slickNav.slick({
            adaptiveHeight: false,
            arrows: false,
            centerMode: false,
            dots: false,
            focusOnSelect: true,
            swipe: true,
            swipeToSlide: true,
            infinite: true,
            centerPadding: '40px',
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            responsive: [
                {
                    breakpoint: 414,
                    settings: {
                        slidesToShow: 4,
                    }
                },
                {
                    breakpoint: 374,
                    settings: {
                        slidesToShow: 3,
                    }
                }
            ]
        });
    }

    function addClassInArrElements(wrap, innerElem, newClass) {
        wrap.find(innerElem).each((index, elem) => {
            $(elem).addClass(newClass);
        })
    }

    function removeClassInArrElements(wrap, innerElem, oldClass) {
        wrap.find(innerElem).each((index, elem) => {
            $(elem).removeClass(oldClass);
        });
    }

    function handleSlickForInit(event, slick){
        const slideActive = $(this).find('.slick-slide.slick-active');

        addClassInArrElements(slideActive, '[data-aos^=fade]', 'aos-animate');

        if ($progress.length) {
            $progress.find('span').stop(true, true).animate({
                width: "100%"
            }, {
                duration: 5000,
                specialEasing: {
                    width: "linear"
                },
                complete: function () {
                    $slickDots.slick('slickNext');
                    $(this).removeAttr('style');
                }
            })
        }
    }

    function handleSlickForSwipe(event, slick, direction){
        const current = $(this).find('.slick-slide.slick-active');
        const next = current.next();
        const prev = current.prev();

        removeClassInArrElements(next, '[data-aos^=fade]', 'aos-animate');
        removeClassInArrElements(prev, '[data-aos^=fade]', 'aos-animate');

        if ($progress.length) {
            $progress.find('span').stop(true, true).animate({
                width: "100%"
            }, {
                duration: 5000,
                specialEasing: {
                    width: "linear"
                },
                complete: function () {
                    $slickDots.slick('slickNext');
                    $(this).removeAttr('style');

                    if ($slickDots.find('.slick-dots').hasClass('fadeOut')) {
                        $slickDots.slick('slickPlay');
                        $slickDots.find('.slick-dots').removeClass('fadeOut').fadeIn(150);
                        $slickDotsBtnGoToEnd.hide();
                        $slickDotsBtnRedirect.removeAttr('hidden');
                        $(this).closest('.progress').fadeIn(150);
                    }
                }
            })
        }
    }

    function handleSlickForBeforeChange(event, slick, currentSlide, nextSlide){
        const allSlides = $(this).find('.slick-slide');

        removeClassInArrElements(allSlides, '[data-aos^=fade]', 'aos-animate');

        if ($progress.length) {
            $progress.find('span').css({
                width: 0
            });
        }
    }

    function handleSlickForAfterChange(event, slick, currentSlide) {
        const current = $(this).find('.slick-slide.slick-active');

        addClassInArrElements(current, '[data-aos^=fade]', 'aos-animate');

        if (slick.$slides.length === currentSlide + 1) {
            $slickDotsBtnGoToEnd.hide();
        }

        if (currentSlide !== slick.$slides.length - 1) {
            if ($progress.length) {
                $progress.find('span').stop(true, true).animate({
                    width: "100%"
                }, {
                    duration: 5000,
                    specialEasing: {
                        width: "linear"
                    },
                    complete: function () {
                        $slickDots.slick('slickNext');
                        $(this).removeAttr('style');

                        if ($slickDots.find('.slick-dots').hasClass('fadeOut')) {
                            $slickDots.slick('slickPlay');
                            $slickDots.find('.slick-dots').removeClass('fadeOut').fadeIn(150);
                            $slickDotsBtnGoToEnd.hide();
                            $slickDotsBtnRedirect.removeAttr('hidden');
                            $(this).closest('.progress').fadeIn(150);
                        }
                    }
                })
            }
        } else {
            $progress.find('span').stop(true, true).animate({
                width: "100%"
            }, {
                duration: 5000,
                specialEasing: {
                    width: "linear"
                },
                complete: function () {
                    $slickDots.slick('slickPause');
                    $slickDotsBtnGoToEnd.hide();
                    $slickDotsBtnRedirect.removeAttr('hidden');
                    $slickDots.find('.slick-dots').addClass('fadeOut').fadeOut(150);
                    $(this).closest('.progress').fadeOut(150);
                }
            })
        }
    }

    if ($timer.length){
        let timer1 = makeCounter;
        let timer2 = makeCounter;
        let timer3 = makeCounter;

        timer1($('[data-timer-display="1"]'), 1, timer1);
        timer2($('[data-timer-display="2"]'), 120, timer2);
        timer3($('[data-timer-display="3"]'), 73, timer3);
    }

    if ($accordion.length) {
        $accordion.addClass('hidden').find('.accordion__body').hide();

        $accordion.on('click', '.accordion__header', function () {
            const self = $(this);
            const $closest = self.closest('.accordion');
            const $accBody = $closest.find('.accordion__body');

            if (!$closest.hasClass('hidden')) {
                $closest.addClass('hidden');
                toggleAnimate($accBody, "hide", 250);
            } else {
                $closest.removeClass('hidden');
                toggleAnimate($accBody, "show", 250);
            }
        });
    }

    if ($letters.length) {
        $letters.on('click', function () {
            const self = $(this);

            if (!self.hasClass('opened')) {
                self.addClass('opened')
            }
        });
    }

    // svg polyfill for old browsers
    svg4everybody();

    // PWA init
    // conditionPWA();

    function makeCounter (elem, time, intervalId) {

        function timer(seconds) {
            // clear any existing timers
            clearInterval(intervalId);

            const now = Date.now();
            const then = now + seconds * 1000;
            displayTimeLeft(seconds);

            intervalId = setInterval(() => {
                const secondsLeft = Math.round((then - Date.now()) / 1000);
                // check if we should stop it!
                if(secondsLeft < 0) {
                    clearInterval(intervalId);
                    return;
                }
                // display it
                displayTimeLeft(secondsLeft);
            }, 1000);
        }

        function displayTimeLeft(seconds){
            const hours = (Math.floor(seconds/3600) < 10 ? "0" : "") + Math.floor(seconds/3600);
            const minutes = ((Math.floor(seconds/60) % 60) < 10 ? "0" : "") + Math.floor(seconds/60) % 60;
            const remainderSeconds = seconds % 60;
            const display = `${hours}:${minutes}:${remainderSeconds < 10 ? '0' : '' }${remainderSeconds}`;``;

            elem.text(display);
        }

        function startTimer() {
            let seconds = time * 60;
            timer(seconds);
        }

        startTimer();
    }

    function counter() {
        $counters.each(function (index, elem) {
            const circle = $(elem).find('.counter__circle-value');

            setTimeout(function () {
                circle.attr('stroke-dashoffset', 0);
            }, 500);

            $(elem).find('.counter__output').each(function () {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 1500,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        });
    }

    function accent() {
        const $circle = $accent.find('.counter__circle-value');
        const $value = $accent.find('.counter__output');

        setTimeout(function () {
            $circle.attr('stroke-dashoffset', 0);
        }, 500);

        $value.prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1500,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    }

    function debounce(f, ms) {

        let isCoolDown = false;

        return function () {
            if (isCoolDown) return;

            f.apply(this, arguments);

            isCoolDown = true;

            setTimeout(() => isCoolDown = false, ms);
        };

    }

    function toggleAnimate(elem, param, speed) {
        elem.animate({
            opacity: param,
            height: param
        }, {
            duration: speed || 500,
            specialEasing: {
                opacity: 'linear',
                height: 'swing'
            }
        })
    }

    function conditionPWA() {
        if (navigator.serviceWorker.controller) {
            console.log('[PWA Builder] active service worker found, no need to register')
        } else {
            navigator.serviceWorker.register('js/sw.js', {
                scope: './'
            }).then(function (reg) {
                console.log('Service worker has been registered for scope:' + reg.scope);
            });
        }
    }

    function addClassToBody(elem, className) {
        if (elem.length) {
            $body.addClass(className);
        }
    }

    function removeClassFromBody(elem, className) {
        if (elem.length) {
            $body.removeClass(className);
        }
    }

})(jQuery);
